# Automatic Device Provisioning

This is a tool for rapid creation and initialization of devices.

Information is inputted through an Excel file. The excel document has a very specific format. 
The most current version of the template Excel file can be found in src/main/resources folder.

***

### Command Line Options

Command line options are the following (**-u** | **-url**), (**-p** | **-path**), (**-un** | **-username**), 
(**-pw** | **-password**), (**-dry** | **-dry_run**)

* **-u** | **-url**: The server host. This is a required option unless executing a dry run. Defaults to null. 
* **-p** | **-path**: The Excel document to be read. It can either be a relative or an absolute path. This is a required option. 
* **-un** | **-username**: Valid username. Defaults to null.
* **-pw** | **-password**: Valid password. Defaults to null.
* **-dry** | **-dry_run**: Indicates that the user does not want to interact with the database. This option does not take any arguments. If used, then **-url** does *NOT* need to be specified.

###### Example: 
```terminal
-u "http://your.url" -p "C:\Your\File\Path\data.xlsx" -un admin -pw 123456 -dry
```

***
### Excel Fields
A template .xlsx file can be found in the *src/main/resources* directory of this repository. File name: *template.xlsx*

* Id (first column) needs to be specified for easier parsing of the document. No specific requirements (can be number or text). 
However, it needs to be unique. If this requirement is not met, the program will throw an error.
* Customer is a column purely for convenience.
* **batchCreateDevice**: Columns related to creating the device are color coded in *green*. 
*Numbers* column should include values that are comma separated.
* **Initial Config**:
    * **LPMDL_1101**: Columns related to initial config are color coded in *yellow*. For 1101 specifically, the *pin* column 
    must be present, while *bands* is not required and will be ignored.
    * **LPMDL_1102**: Columns related to initial config are color coded in *yellow*. For 1102 specifically, the *bands* column 
    must be present, while *pin* is not required and will be ignored.
    * **LPMDL_1103**: Columns related to initial config are color coded in *red*.
    
