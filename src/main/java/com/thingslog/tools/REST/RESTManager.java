package com.thingslog.tools.REST;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class RESTManager {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_CYAN = "\u001B[36m";
    private String baseURL;
    private String bearerToken;

    public RESTManager(String baseURL, String username, String password) throws IOException, JSONException {
        this.baseURL = baseURL;
        this.bearerToken = getBearerToken(username, password);
    }

    public void BatchCreateDevices(JSONObject jsonObject) throws IOException, JSONException {
        // Create connection to URL
        URL url = new URL (baseURL + "/devices/batch");
        HttpURLConnection con = (HttpURLConnection)url.openConnection();
        con.setRequestMethod("POST");
        con.setDoOutput(true);

        // Attach Headers
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Authorization", bearerToken);

        // Creates JSON String
        var jsonInputString = jsonObject.toString();

        // Writes JSON String
        try(OutputStream os = con.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        // Get the input stream to read the response content
        try(BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            // Read through the whole response content, and print the final response string
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println("batchCreeateDevice Response: "+ response.toString());
        }
    }

    public void updateDeviceInitialConfig(String id, JSONObject currentJSON) throws IOException, JSONException {
        // Create connection to URL
        URL url = new URL (baseURL + "/device/" + id + "/initial-config");
        HttpURLConnection con = (HttpURLConnection)url.openConnection();
        con.setRequestMethod("PUT");
        con.setDoOutput(true);

        // Attach Headers
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Authorization", bearerToken);

        // Creates JSON String
        var jsonInputString = currentJSON.toString();

        // Writes JSON String
        try(OutputStream os = con.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        // Get the input stream to read the response content
        try(BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            // Read through the whole response content, and print the final response string
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println(id +": Initial Config Successful.");
        }
    }

    private String getBearerToken(String username, String password) throws IOException, JSONException {
            // Create connection to URL
            URL url = new URL (baseURL + "/login");
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("POST");
            con.setDoOutput(true);

            // Attach Headers
            con.setRequestProperty("Content-Type", "application/json; utf-8");

            // Create authentication JSON Object
            JSONObject jo = new JSONObject();
            jo.put("password", password);
            jo.put("username", username);
            var jsonInputString = jo.toString();

        try {
            // Writes JSON String
            try(OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            // Returns Bearer token found in the response headers
            return con.getHeaderFields().get("Authorization").get(0);
        } catch (Exception e) {
            throw new IOException("Cannot establish a connection to the server.\n" +
                    "Solution 1: Revise -un and/or -pw cmd options.\n" +
                    "Solution 2: Revise -url cmd option.");
        }
    }

    public JSONObject getInitialConfigFor(String id) throws IOException, JSONException {
        URL url = new URL(baseURL + "/device/" + id + "/initial-config");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Authorization", bearerToken);

        con.setConnectTimeout(5000);
        con.setReadTimeout(5000);

        // Get the input stream to read the response content
        try(BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            // Read through the whole response content, and print the final response string
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            return new JSONObject(response.toString());
        }
    }
}
