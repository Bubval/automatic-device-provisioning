package com.thingslog.tools.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BatchCreateDevices {
    public String description;
    public String hwVersion;
    public String model;
    public JSONArray numbers;
    public String swVersion;

    public JSONObject toJSON() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put("description", description);
        jo.put("hwVersion", hwVersion);
        jo.put("model", model);
        jo.put("numbers", numbers);
        jo.put("swVersion", swVersion);
        return jo;
    }
}

