package com.thingslog.tools.models;


import lombok.Data;

@Data
public class ExcelModel {
    private String id;
    private String consumer;
    private String numbers;
    private String model;
    private String description;
    private String softwareVersion;
    private String hardwareVersion;
    private String address;
    private int port;
    private String apn;
    private String pin;
    private Boolean resolveAddress;
    private String simNumber;
    private String simProvider;
    private String bands;
    private String adr;
    private int dr;
    private String feq;
    private int rx2Chan;
    private String rx2Freq;
    private String appkey;
    private String appeui;
    private String deveui;
    private String appskey;
    private String nwkskey;
    private String devaddr;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getConsumer() {
        return consumer;
    }
    public void setConsumer(String consumer) {
        this.consumer = consumer;
    }

    public String getNumbers() {
        return numbers;
    }
    public void setNumbers(String numbers) {
        this.numbers = numbers;
    }

    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getSoftwareVersion() {
        return softwareVersion;
    }
    public void setSoftwareVersion(String softwareVersion) {
        this.softwareVersion = softwareVersion;
    }

    public String getHardwareVersion() {
        return hardwareVersion;
    }
    public void setHardwareVersion(String hardwareVersion) {
        this.hardwareVersion = hardwareVersion;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public int getPort() {
        return port;
    }
    public void setPort(int port) {
        this.port = port;
    }

    public String getApn() {
        return apn;
    }
    public void setApn(String apn) {
        this.apn = apn;
    }

    public String getPin() {
        return pin;
    }
    public void setPin(String pin) {
        this.pin = pin;
    }

    public Boolean getResolveAddress() {
        return resolveAddress;
    }
    public void setResolveAddress(Boolean resolveAddress) {
        this.resolveAddress = resolveAddress;
    }

    public String getSimNumber() {
        return simNumber;
    }
    public void setSimNumber(String simNumber) {
        this.simNumber = simNumber;
    }

    public String getSimProvider() {
        return simProvider;
    }
    public void setSimProvider(String simProvider) {
        this.simProvider = simProvider;
    }

    public String getBands() {
        return bands;
    }
    public void setBands(String bands) {
        this.bands = bands;
    }

    public String getAdr() {
        return adr;
    }
    public void setAdr(String adr) {
        this.adr = adr;
    }

    public int getDr() {
        return dr;
    }
    public void setDr(int dr) {
        this.dr = dr;
    }

    public String getFeq() {
        return feq;
    }
    public void setFeq(String feq) {
        this.feq = feq;
    }

    public int getRx2Chan() {
        return rx2Chan;
    }
    public void setRx2Chan(int rx2Chan) {
        this.rx2Chan = rx2Chan;
    }

    public String getRx2Freq() {
        return rx2Freq;
    }
    public void setRx2Freq(String rx2Freq) {
        this.rx2Freq = rx2Freq;
    }

    public String getAppkey() {
        return appkey;
    }
    public void setAppkey(String appkey) { this.appkey = appkey; }

    public String getAppeui() {
        return appeui;
    }
    public void setAppeui(String appeui) {
        this.appeui = appeui;
    }

    public String getDeveui() {
        return deveui;
    }
    public void setDeveui(String deveui) {
        this.deveui = deveui;
    }

    public String getAppskey() {
        return appskey;
    }
    public void setAppskey(String appskey) {
        this.appskey = appskey;
    }

    public String getNwkskey() {
        return nwkskey;
    }
    public void setNwkskey(String nwkskey) {
        this.nwkskey = nwkskey;
    }

    public String getDevaddr() { return devaddr; }
    public void setDevaddr(String devaddr) {
        this.devaddr = devaddr;
    }
}
