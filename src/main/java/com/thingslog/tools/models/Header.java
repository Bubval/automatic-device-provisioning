package com.thingslog.tools.models;

import lombok.Data;

@Data
public class Header {
    public static final String ID = "id";
    public static final String CONSUMER = "Consumer";
    public static final String NUMBERS = "Numbers";
    public static final String MODEL = "Model";
    public static final String DESCRIPTION = "Description";
    public static final String SOFTWARE_VERSION = "Software Version";
    public static final String HARDWARE_VERSION = "Hardware Version";
    public static final String ADDRESS = "address";
    public static final String PORT = "port";
    public static final String APN = "apn";
    public static final String PIN = "pin";
    public static final String BANDS = "bands";
    public static final String RESOLVE_ADDRESS = "resolve address";
    public static final String SIM_NUMBER = "sim card number";
    public static final String SIM_PROVIDER = "sim card provider";
    public static final String ADR = "adr";
    public static final String DR = "dr";
    public static final String FREQ = "freq";
    public static final String RX2CHAN = "rx2Chan";
    public static final String RX2FREQ = "rx2Freq";
    public static final String APPKEY = "Config appkey";
    public static final String APPEUI = "Config appeui";
    public static final String DEVEUI = "Config deveui";
    public static final String APPSKEY = "Collect appskey";
    public static final String NWKSKEY = "Collect nwkskey";
    public static final String DEVADDR = "Collect devaddr";

    int cIndex;
    int rIndex;
    String name;

    public Header(int cIndex, int rIndex, String name) {
        this.cIndex = cIndex;
        this.rIndex = rIndex;
        this.name = name;
    }

    public int getCIndex() {
        return cIndex;
    }
    public int getRIndex() {
        return rIndex;
    }
    public String getName() {
        return name;
    }
}
