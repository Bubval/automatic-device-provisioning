package com.thingslog.tools;

import com.thingslog.tools.models.ExcelModel;
import com.thingslog.tools.models.Header;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.*;
import java.util.*;

public class Helpers {
    public static CellType getCellType(String id, String h, Sheet sheet, List<Header> headers) {
        String currentId = null;

        int firstRow = headers.get(0).getRIndex() + 2;

        // Iterate rows
        for (int i = firstRow; i < sheet.getLastRowNum() + 1; i++) {
            Row row = sheet.getRow(i);

            // Iterate columns
            for (Header header : headers) {
                // Get ID of current row
                if (header.getName().equals(Header.ID)) {
                    currentId = row.getCell(header.getCIndex()).getStringCellValue();
                }
                // If current column is the one we specified (h)
                if (header.getName().equals(h)) {
                    // If the current row has specified id (id)
                    if (currentId.equals(id)) {
                        // Returns cell type
                        return row.getCell(header.getCIndex()).getCellType();
                    }
                }
            }
        }
        return null;
    }

    public static boolean fileExists(String path, String extension) {
        File file = new File(path);
        String fileName = FilenameUtils.getName(path);
        return file.exists() && !file.isDirectory() && FilenameUtils.isExtension(fileName, extension);
    }

    public static boolean isValidURL(String url) {
        try {
            new URL(url).toURI();
        } catch (MalformedURLException | URISyntaxException e) {
            return false;
        }
        return true;
    }

    public static JSONObject getLPMDL1101 (JSONObject object, ExcelModel model) throws JSONException {
        object.put("address", model.getAddress());
        object.put("port", model.getPort());
        object.put("apn", model.getApn());
        object.put("pin", model.getPin());
        object.put("resolveAddress", model.getResolveAddress());
        object.put("simCardNumber", model.getSimNumber());
        object.put("simCardProvider", model.getSimProvider());
        return object;
    }

    public static JSONObject getLPMDL1102 (JSONObject object, ExcelModel model) throws JSONException {
        object.put("address", model.getAddress());
        object.put("port", model.getPort());
        object.put("apn", model.getApn());
        object.put("bands", model.getBands());
        object.put("resolveAddress", model.getResolveAddress());
        object.put("simCardNumber", model.getSimNumber());
        object.put("simCardProvider", model.getSimProvider());
        return object;
    }

    public static JSONObject getLPMDL1103 (JSONObject object, ExcelModel model) throws JSONException {

        JSONObject appConfig = new JSONObject();
        appConfig.put("appkey", model.getAppkey());
        appConfig.put("appeui", model.getAppeui());
        appConfig.put("deveui", model.getDeveui());

        JSONObject appCollect = new JSONObject();
        appCollect.put("appskey", model.getAppskey());
        appCollect.put("nwkskey", model.getNwkskey());
        appCollect.put("devaddr", model.getDevaddr());

        object.put("appConfig", appConfig);
        object.put("appCollect", appCollect);
        object.put("adr", model.getAdr());
        object.put("dr", model.getDr());
        object.put("freq", model.getFeq());
        object.put("rx2Chan", model.getRx2Chan());
        object.put("rx2Freq", model.getRx2Freq());

        return object;
    }

    public static Set<String> findDuplicates(List<String> ids) {
        final Set<String> duplicates  = new HashSet<>();
        final Set<String> set = new HashSet<>();

        for (String id : ids)
        {
            if (!set.add(id))
            {
                duplicates.add(id);
            }
        }
        return duplicates;
    }
}
