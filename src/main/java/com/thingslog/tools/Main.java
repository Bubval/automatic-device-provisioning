package com.thingslog.tools;

import com.google.gson.Gson;
import com.thingslog.tools.REST.RESTManager;
import com.thingslog.tools.models.BatchCreateDevices;
import com.thingslog.tools.models.ExcelModel;
import org.apache.commons.cli.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public class Main {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_GREEN = "\u001B[32m";

    public static void main(String[] args) {
        Options options = new Options();
        options.addOption("help", "help", false, "Print command help.");
        options.addOption("p", "path", true, "The excel document to be read. Path can be relative or absolute. Defaults to null.'");
        options.addOption("dry","dry_run",false,"Dry run without touching the database" );
        options.addOption("un", "username", true, "Username");
        options.addOption("pw", "password", true, "Password");
        options.addOption("u", "url", true, "Server host. Default to null.");

        CommandLineParser parser = new DefaultParser();

        try {
            // Retrieves all options passed by command line
            CommandLine line = parser.parse( options, args );
            if (line.hasOption("help") || line.getOptions().length==0) {
                new HelpFormatter().printHelp("Automatic Device Provisioning",  options);
                return;
            }

            String path = line.getOptionValue("p", null);
            String username = line.getOptionValue("un", null);
            String password = line.getOptionValue("pw", null);
            String url = line.getOptionValue("u", null);

            boolean dryFlag = false;
            if (line.hasOption("dry")) { dryFlag = true; }
            boolean usernameFlag = false;
            if (line.hasOption("un")) { usernameFlag = true; }
            boolean passwordFlag = false;
            if (line.hasOption("pw")) { passwordFlag = true; }

            // Check if command line options are valid
            String absolutePath = new File(path).getCanonicalPath();
            if (!Helpers.fileExists(absolutePath, "xlsx")) {
                throw new IOException("No valid .xlsx document can be found at specified file path: (" + absolutePath + ")" +
                        "\nSolution: Update command line option -p | -path. Current value: " + path);
            }

            if (username == null || !usernameFlag || password == null || !passwordFlag) {
                throw new ParseException(
                        "Command line options for username and/or password missing.\n" +
                        "Solution: Update command line option -un and/or -pw.\n" +
                        "Current Value: username (" + username + "), password(" + password + ")."
                );
            }

            if (!Helpers.isValidURL(url) && !dryFlag) {
                throw new IllegalArgumentException(
                        "Specified URL (" + url + ") is not recognized as a valid URL.\n" +
                        "Solution: Update command line option -u | -url. Current value: " + url
                );
            }

            // Retrieves all data from excel file
            ExcelReader excelReader = new ExcelReader(absolutePath);
            List<ExcelModel> models;
            try {
                models = excelReader.getRows();
            } catch (FileNotFoundException e) {
                throw new FileNotFoundException("File not found.");
            }

            List<String> ids = models.stream().map(ExcelModel::getId).collect(Collectors.toList());
            Set<String> duplicates = Helpers.findDuplicates(ids);
            if(duplicates.size() > 0) {
                throw new IOException("Could not execute because of the following duplicate ids: "+ duplicates +"\n" +
                        "Solution: Make sure to change the value of rows with duplicate ids.");
            }

            RESTManager restManager = (dryFlag) ? null : new RESTManager(url, username, password);

            // Iterate through all rows in Excel and create devices
            for(ExcelModel model : models) {
                BatchCreateDevices batchCreateDevices = new BatchCreateDevices();
                batchCreateDevices.description = model.getDescription();
                batchCreateDevices.hwVersion = model.getHardwareVersion();
                batchCreateDevices.model = model.getModel();
                batchCreateDevices.numbers = new JSONArray(model.getNumbers().split(","));
                batchCreateDevices.swVersion = model.getSoftwareVersion();

                System.out.println("DEVICE(S) CREATED: " + new Gson().toJson(batchCreateDevices));

                if (!dryFlag) {
                    // Runs batchCreateDevices
                    restManager.BatchCreateDevices(batchCreateDevices.toJSON());

                    // Deduce if current model is LPMDL_1101
                    if (model.getModel().equalsIgnoreCase("LPMDL_1101")) {
                        // Iterate through each number (device id)
                        for(String id : model.getNumbers().split(",")){
                            // Generate a new JSON object containing all required init config properties
                            JSONObject putObj = Helpers.getLPMDL1101(
                                    restManager.getInitialConfigFor(id),
                                    model
                            );
                            // Execute PUT
                            restManager.updateDeviceInitialConfig(id, putObj);
                        }
                    }
                    // Deduce if current model is LPMDL_1102
                    else if (model.getModel().equalsIgnoreCase("LPMDL_1102")) {
                        // Iterate through each number (device id)
                        for(String id : model.getNumbers().split(",")){
                            // Generate a new JSON object containing all required init config properties
                            JSONObject putObj = Helpers.getLPMDL1102(
                                    restManager.getInitialConfigFor(id),
                                    model
                            );
                            // Execute PUT
                            restManager.updateDeviceInitialConfig(id, putObj);
                        }
                    }
                    // Deduce if current model is LPMDL_1103 or BL_100_L or W1
                    else if (model.getModel().equalsIgnoreCase("LPMDL_1103") ||
                            model.getModel().equalsIgnoreCase("BL_100_L")  ||
                            model.getModel().equalsIgnoreCase("W1") ) {
                        // Iterate through each number (device id)
                        for(String id: model.getNumbers().split(",")){
                            // Generate a new JSON object containing all required init config properties
                            JSONObject putObj = Helpers.getLPMDL1103(
                                    restManager.getInitialConfigFor(id),
                                    model
                            );
                            // Execute PUT
                            restManager.updateDeviceInitialConfig(id, putObj);
                        }
                    }
                }
            }
            if (dryFlag) { System.err.println("DRY RUN COMPLETE: No database interaction. Remove -dry from CMD options."); }
        }
        catch( Exception e ) {
            System.err.println( "Failed.\nReason: " + e.getMessage() );
        }
    }
}
