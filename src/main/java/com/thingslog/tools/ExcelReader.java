package com.thingslog.tools;

import com.thingslog.tools.models.ExcelModel;
import com.thingslog.tools.models.Header;
import org.apache.poi.ss.usermodel.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ExcelReader {
    private final String path;

    public ExcelReader(String path) {
        this.path = path;
    }

    public List<ExcelModel> getRows() throws FileNotFoundException {
        List<ExcelModel> excelModels;

        InputStream inputStream = new FileInputStream(path);
        try (Workbook workbook = WorkbookFactory.create(inputStream)) {
            var stylesheet = workbook.getSheetAt(0);
            List<Header> headers = getHeaders(stylesheet);
            excelModels = getRows(stylesheet, headers);
            checkForCorrectCellTypes(excelModels, stylesheet, headers);
        } catch (IOException e) {
            throw new FileNotFoundException("Unable to read data the from stream");
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Unable to read data from stream. " + e.getMessage());
        }

        return excelModels;
    }

    private List<ExcelModel> getRows(Sheet sheet, List<Header> headers) {
        List<ExcelModel> excelModels = new LinkedList<>();

        // Second row after the header
        int firstRow = headers.get(0).getRIndex() + 2;

        for (int i = firstRow; i < sheet.getLastRowNum() + 1; i++)  {
            Row row = sheet.getRow(i);
            ExcelModel excelModel = new ExcelModel();
            String model = "";

            for (Header header : headers) {
                try {

                    if (header.getName().trim().equals(Header.ID)) {
                        if ( row.getCell(header.getCIndex()).getCellType() == CellType.NUMERIC ) {
                            excelModel.setId(Integer.toString((int) row.getCell(header.getCIndex()).getNumericCellValue()));
                        } else {
                            excelModel.setId(row.getCell(header.getCIndex()).getStringCellValue());
                        }
                    }


                    // Parse common params for batchCreateDevice
                    if (header.getName().equals(Header.CONSUMER)) {
                        excelModel.setConsumer(row.getCell(header.getCIndex()).getStringCellValue());
                    }
                    if (header.getName().equals(Header.NUMBERS)) {
                        // Removes whitespace from string.
                        String numbers = row.getCell(header.getCIndex()).getStringCellValue();
                        excelModel.setNumbers(numbers.replaceAll("\\s+", ""));
                    }
                    if (header.getName().equals(Header.MODEL)) {
                        // Remove whitespace from string.
                        model = row.getCell(header.getCIndex()).getStringCellValue();
                        excelModel.setModel(model.replaceAll("\\s+", ""));
                    }
                    if (header.getName().equals(Header.DESCRIPTION)) {
                        excelModel.setDescription(row.getCell(header.getCIndex()).getStringCellValue());
                    }
                    if (header.getName().equals(Header.SOFTWARE_VERSION)) {
                        excelModel.setSoftwareVersion(row.getCell(header.getCIndex()).getStringCellValue());
                    }
                    if (header.getName().equals(Header.HARDWARE_VERSION)) {
                        excelModel.setHardwareVersion(row.getCell(header.getCIndex()).getStringCellValue());
                    }


                    // Parse common params for LPMDL_1101 & LPMDL_1102
                    if (model.equalsIgnoreCase("LPMDL_1101") || model.equalsIgnoreCase("LPMDL_1102")) {
                        if (header.getName().equals(Header.ADDRESS)) {
                            excelModel.setAddress(row.getCell(header.getCIndex()).getStringCellValue());
                        }
                        if (header.getName().equals(Header.PORT)) {
                            excelModel.setPort(Integer.parseInt(row.getCell(header.getCIndex()).getStringCellValue()));
                        }
                        if (header.getName().equals(Header.APN)) {
                            excelModel.setApn(row.getCell(header.getCIndex()).getStringCellValue());
                        }
                        if (header.getName().equals(Header.RESOLVE_ADDRESS)) {
                            excelModel.setResolveAddress(row.getCell(header.getCIndex()).getBooleanCellValue());
                        }
                        if (header.getName().equals(Header.SIM_NUMBER)) {
                            excelModel.setSimNumber(row.getCell(header.getCIndex()).getStringCellValue());
                        }
                        if (header.getName().equals(Header.SIM_PROVIDER)) {
                            excelModel.setSimProvider(row.getCell(header.getCIndex()).getStringCellValue());
                        }
                    }


                    // Parse specific LPMDL_1101 params
                    if (model.equalsIgnoreCase("LPMDL_1101")) {
                        if (header.getName().equals(Header.PIN)) {
                            excelModel.setPin(row.getCell(header.getCIndex()).getStringCellValue());
                        }
                    }
                    // Parse specific LPMDL_1102 params
                    if (model.equalsIgnoreCase("LPMDL_1102")) {
                        if (header.getName().equals(Header.BANDS)) {
                            excelModel.setBands(row.getCell(header.getCIndex()).getStringCellValue());
                        }
                    }


                    // Parse specific LPMDL_1103 params
                    if (model.equalsIgnoreCase("LPMDL_1103")) {
                        if (header.getName().equals(Header.ADR)) {
                            excelModel.setAdr(row.getCell(header.getCIndex()).getStringCellValue());
                        }
                        if (header.getName().equals(Header.DR)) {
                            excelModel.setDr(Integer.parseInt(row.getCell(header.getCIndex()).getStringCellValue()));
                        }
                        if (header.getName().equals(Header.FREQ)) {
                            excelModel.setFeq(row.getCell(header.getCIndex()).getStringCellValue());
                        }
                        if (header.getName().equals(Header.RX2CHAN)) {
                            excelModel.setRx2Chan(Integer.parseInt(row.getCell(header.getCIndex()).getStringCellValue()));
                        }
                        if (header.getName().equals(Header.RX2FREQ)) {
                            excelModel.setRx2Freq(row.getCell(header.getCIndex()).getStringCellValue());
                        }
                        if (header.getName().equals(Header.APPKEY)) {
                            excelModel.setAppkey(row.getCell(header.getCIndex()).getStringCellValue());
                        }
                        if (header.getName().equals(Header.APPEUI)) {
                            excelModel.setAppeui(row.getCell(header.getCIndex()).getStringCellValue());
                        }
                        if (header.getName().equals(Header.DEVEUI)) {
                            excelModel.setDeveui(row.getCell(header.getCIndex()).getStringCellValue());
                        }
                        if (header.getName().equals(Header.APPSKEY)) {
                            excelModel.setAppskey(row.getCell(header.getCIndex()).getStringCellValue());
                        }
                        if (header.getName().equals(Header.NWKSKEY)) {
                            excelModel.setNwkskey(row.getCell(header.getCIndex()).getStringCellValue());
                        }
                        if (header.getName().equals(Header.DEVADDR)) {
                            excelModel.setDevaddr(row.getCell(header.getCIndex()).getStringCellValue());
                        }
                    }
                }
                catch (NullPointerException | IllegalStateException npe) {
                    System.err.println("Unable to parse some of the values from row: " + (i + 1));
                }
            }
            excelModels.add(excelModel);
        }
        return excelModels;
    }

    private List<Header> getHeaders(Sheet sheet) {
        List<Header> headers = new LinkedList<>();

        Row row;
        Cell cell;

        Iterator rows = sheet.rowIterator();
        while (rows.hasNext()) {
            row = (Row) rows.next();
            Iterator cells = row.cellIterator();

            while (cells.hasNext()) {
                cell = (Cell) cells.next();

                if (
                        cell.toString().equalsIgnoreCase(Header.ID) ||
                        cell.toString().equalsIgnoreCase(Header.CONSUMER) ||
                        cell.toString().equalsIgnoreCase(Header.NUMBERS) ||
                        cell.toString().equalsIgnoreCase(Header.MODEL) ||
                        cell.toString().equalsIgnoreCase(Header.DESCRIPTION) ||
                        cell.toString().equalsIgnoreCase(Header.SOFTWARE_VERSION) ||
                        cell.toString().equalsIgnoreCase(Header.HARDWARE_VERSION) ||
                        cell.toString().equalsIgnoreCase(Header.ADDRESS) ||
                        cell.toString().equalsIgnoreCase(Header.PORT) ||
                        cell.toString().equalsIgnoreCase(Header.APN) ||
                        cell.toString().equalsIgnoreCase(Header.PIN) ||
                        cell.toString().equalsIgnoreCase(Header.RESOLVE_ADDRESS) ||
                        cell.toString().equalsIgnoreCase(Header.SIM_NUMBER) ||
                        cell.toString().equalsIgnoreCase(Header.SIM_PROVIDER) ||
                        cell.toString().equalsIgnoreCase(Header.BANDS) ||
                        cell.toString().equalsIgnoreCase(Header.ADR) ||
                        cell.toString().equalsIgnoreCase(Header.DR) ||
                        cell.toString().equalsIgnoreCase(Header.FREQ) ||
                        cell.toString().equalsIgnoreCase(Header.RX2CHAN) ||
                        cell.toString().equalsIgnoreCase(Header.RX2FREQ) ||
                        cell.toString().equalsIgnoreCase(Header.APPKEY) ||
                        cell.toString().equalsIgnoreCase(Header.APPEUI) ||
                        cell.toString().equalsIgnoreCase(Header.DEVEUI) ||
                        cell.toString().equalsIgnoreCase(Header.APPSKEY) ||
                        cell.toString().equalsIgnoreCase(Header.NWKSKEY) ||
                        cell.toString().equalsIgnoreCase(Header.DEVADDR)
                ) {
                    headers.add(new Header(cell.getColumnIndex(), cell.getRowIndex(), cell.toString()));
                }
            }
        }
        return headers;
    }

    private void checkForCorrectCellTypes(List<ExcelModel> excelModels, Sheet sheet, List<Header> headers) {
        int index = 2;
        for (ExcelModel model : excelModels) {
            index++;
            if (Helpers.getCellType(model.getId(), Header.ID, sheet, headers) != CellType.STRING) {
                throw new IllegalArgumentException("Could not parse ID on device with ID '" + model.getId() + "' found on row " + index + ".");
            }
            if (Helpers.getCellType(model.getId(), Header.CONSUMER, sheet, headers) != CellType.STRING) {
                throw new IllegalArgumentException("Could not parse CONSUMER on device with ID '" + model.getId() + "' found on row " + index + ".");
            }
            if (Helpers.getCellType(model.getId(), Header.NUMBERS, sheet, headers) != CellType.STRING) {
                throw new IllegalArgumentException("Could not parse NUMBERS on device with ID '" + model.getId() + "' found on row " + index + ".");
            }
            if (Helpers.getCellType(model.getId(), Header.MODEL, sheet, headers) != CellType.STRING) {
                throw new IllegalArgumentException("Could not parse MODEL on device with ID '" + model.getId() + "' found on row " + index + ".");
            }
            if (Helpers.getCellType(model.getId(), Header.DESCRIPTION, sheet, headers) != CellType.STRING) {
                throw new IllegalArgumentException("Could not parse DESCRIPTION on device with ID '" + model.getId() + "' found on row " + index + ".");
            }
            if (Helpers.getCellType(model.getId(), Header.SOFTWARE_VERSION, sheet, headers) != CellType.STRING) {
                throw new IllegalArgumentException("Could not parse SOFTWARE VERSION on device with ID '" + model.getId() + "' found on row " + index + ".");
            }
            if (Helpers.getCellType(model.getId(), Header.HARDWARE_VERSION, sheet, headers) != CellType.STRING) {
                throw new IllegalArgumentException("Could not parse HARDWARE VERSION on device with ID '" + model.getId() + "' found on row " + index + ".");
            }

            var currentModel = model.getModel();

            if (currentModel.equals("LPMDL_1101") || currentModel.equals("LPMDL_1102")) {
                if (Helpers.getCellType(model.getId(), Header.ADDRESS, sheet, headers) != CellType.STRING) {
                    throw new IllegalArgumentException("Could not parse ADDRESS on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
                if (Helpers.getCellType(model.getId(), Header.PORT, sheet, headers) != CellType.STRING) {
                    throw new IllegalArgumentException("Could not parse PORT on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
                if (Helpers.getCellType(model.getId(), Header.APN, sheet, headers) != CellType.STRING) {
                    throw new IllegalArgumentException("Could not parse APN on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
                if (Helpers.getCellType(model.getId(), Header.RESOLVE_ADDRESS, sheet, headers) != CellType.BOOLEAN) {
                    throw new IllegalArgumentException("Could not parse RESOLVE ADDRESS on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
                if (Helpers.getCellType(model.getId(), Header.SIM_NUMBER, sheet, headers) != CellType.STRING) {
                    throw new IllegalArgumentException("Could not parse SIM CARD NUMBER on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
                if (Helpers.getCellType(model.getId(), Header.SIM_PROVIDER, sheet, headers) != CellType.STRING) {
                    throw new IllegalArgumentException("Could not parse SIM CARD PROVIDER on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
            }

            if (currentModel.equals("LPMDL_1101")) {
                if (Helpers.getCellType(model.getId(), Header.PIN, sheet, headers) != CellType.STRING) {
                    throw new IllegalArgumentException("Could not parse PIN on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
            }
            if (currentModel.equals("LPMDL_1102")) {
                if (Helpers.getCellType(model.getId(), Header.BANDS, sheet, headers) != CellType.STRING) {
                    throw new IllegalArgumentException("Could not parse BANDS on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
            }

            if(currentModel.equals("LPMDL_1103") || currentModel.equals("W1") || currentModel.equals("BL_100_L ")) {
                if (Helpers.getCellType(model.getId(), Header.ADR, sheet, headers) != CellType.STRING) {
                    throw new IllegalArgumentException("Could not parse ADR on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
                if (Helpers.getCellType(model.getId(), Header.DR, sheet, headers) != CellType.STRING) {
                    throw new IllegalArgumentException("Could not parse DR on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
                if (Helpers.getCellType(model.getId(), Header.FREQ, sheet, headers) != CellType.STRING) {
                    throw new IllegalArgumentException("Could not parse FREQ on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
                if (Helpers.getCellType(model.getId(), Header.RX2CHAN, sheet, headers) != CellType.STRING) {
                    throw new IllegalArgumentException("Could not parse RX2CHAN on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
                if (Helpers.getCellType(model.getId(), Header.RX2FREQ, sheet, headers) != CellType.STRING) {
                    throw new IllegalArgumentException("Could not parse RX2FREQ on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
                if (Helpers.getCellType(model.getId(), Header.APPKEY, sheet, headers) != CellType.STRING) {
                    throw new IllegalArgumentException("Could not parse CONFIG APPKEY on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
                if (Helpers.getCellType(model.getId(), Header.APPEUI, sheet, headers) != CellType.STRING) {
                    throw new IllegalArgumentException("Could not parse CONFIG APPEUI on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
                if (Helpers.getCellType(model.getId(), Header.DEVEUI, sheet, headers) != CellType.STRING) {
                    throw new IllegalArgumentException("Could not parse CONFIG DEVEUI on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
                if (Helpers.getCellType(model.getId(), Header.APPSKEY, sheet, headers) != CellType.STRING) {
                    throw new IllegalArgumentException("Could not parse COLLECT APPSKEY on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
                if (Helpers.getCellType(model.getId(), Header.NWKSKEY, sheet, headers) != CellType.STRING) {
                    throw new IllegalArgumentException("Could not parse COLLECT NWKSKEY on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
                if (Helpers.getCellType(model.getId(), Header.DEVADDR, sheet, headers) != CellType.STRING) {
                    throw new IllegalArgumentException("Could not parse COLLECT DEVADDR on device with ID '" + model.getId() + "' found on row " + index + ".");
                }
            }
        }
    }
}
